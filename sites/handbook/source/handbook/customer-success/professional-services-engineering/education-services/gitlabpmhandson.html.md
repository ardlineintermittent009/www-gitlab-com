---
layout: handbook-page-toc
title: "GitLab Project Management Hands On Guide"
description: "This Hands On Guide is designed to walk you through the lab exercises used in the GitLab Project Management course."
---
# GitLab Project Management Hands On Guide
{:.no_toc}


## GitLab Project Management Labs
* [Lab 1- Review an Example Issue Section](https://about.gitlab.com/handbook/customer-success/professional-services-engineering/education-services/gitlabpmhandsonlab1.html)
* [Lab 2- Create a Sub-Group and Project](https://about.gitlab.com/handbook/customer-success/professional-services-engineering/education-services/gitlabpmhandsonlab2.html)
* [Lab 3- Create an Issue and Add Details to it](https://about.gitlab.com/handbook/customer-success/professional-services-engineering/education-services/gitlabpmhandsonlab3.html)
* [Lab 4- Create a Parent Epic and Sub-Epics](https://about.gitlab.com/handbook/customer-success/professional-services-engineering/education-services/gitlabpmhandsonlab4.html)
* [Lab 5- Create and Assign Milestones](https://about.gitlab.com/handbook/customer-success/professional-services-engineering/education-services/gitlabpmhandsonlab5.html)
* [Lab 6- Create a Wiki Page](https://about.gitlab.com/handbook/customer-success/professional-services-engineering/education-services/gitlabpmhandsonlab6.html)
* [Lab 7- Create a New Board](https://about.gitlab.com/handbook/customer-success/professional-services-engineering/education-services/gitlabpmhandsonlab7.html)

## Quick links

Here are some quick links that may be useful when reviewing this Hands On Guide.

* [GitLab Project Management Course Description](https://about.gitlab.com/services/education/pm/)
* [GitLab Project Management Specialist Certifcation Details](https://about.gitlab.com/services/education/gitlab-project-management-specialist/)


### SUGGESTIONS?

If you wish to make a change to our Hands on Guide for GitLab Project Management- please submit your changes via Merge Request!

