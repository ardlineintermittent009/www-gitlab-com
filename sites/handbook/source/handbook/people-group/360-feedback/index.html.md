---
layout: handbook-page-toc
title: 360 Feedback
description: >-
  360 Feedback is an opportunity where managers, direct reports, and cross
  functional team members will give feedback to each other.
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## 360 Feedback

360 Feedback is an opportunity where managers, direct reports, and cross functional team members will give feedback to each other. There will not be ratings associated with the feedback. This is about investing in each other to help our team members succeed and grow, while also receiving valuable feedback for our own development.

**Next 360 Feedback Cycle**

In Q3 FY'22 we will launch our next 360 feedback cycle. From the People Group we encourage team members to gather feedback from peers, their manager and team members.

### Timeline FY22 

| Start Date  | End Date   | Event                                      | Length  |
|-------------|------------|--------------------------------------------|---------|
| 2021-07-28  | -          | AMA                                        |         |
| 2021-08-02  | -          | Launch Date                                |         |
| 2021-08-02  | 2021-08-15 | Nomination Phase                      | 14      |
| 2021-08-16  | 2021-09-05 | Feedback Phase                          | 21      |
| 2021-09-06  | 2021-09-12 | Review Phase            | 7       |
| 2021-09-13  | 2021-09-27 | Discussion Phase  | 14       |



- August 2nd, 2021 - Survey Launch
- **Nomination Phase** (2 weeks) - Team member nominates peers, manager, and any direct reports they have. Managers can adjust the peer nominations at this time and whenever the nominations are set the manager or a Culture Amp admin can launch the individual review.
- **Feedback Phase** (3 weeks) - Team member, managers, and peers complete reviews for themselves and each other. Managers will have visibility into all self and peer reviews for their direct reports as they are completed.
- **Review Phase** (1 week) -  at the close of the 360 feedback session CultureAmp will send the team member and the manager the report for review.
- **Discussion Phase** (2 Weeks) - Manager and team member schedule a 360 [feedback meeting](https://about.gitlab.com/handbook/people-group/360-feedback/#360-feedback-meeting).
- September 27th  - Closing the cycle
- Date TBD - Feedback Retrospective info to be provided. [(see 2020 retrospective)](https://gitlab.com/gitlab-com/people-group/General/-/issues/617)

_Please note: for 360 feedback cycle we will only include team members with a hire date of before June 1st in the automatic email communications. If you are a team member with a hire date of post June 1st and you want to be added, please reach out to [People Connect](/handbook/people-group/people-connect/) to be manually added._

### 360 via Culture Amp 

We utilize the tool [Culture Amp](https://gitlab.cultureamp.com/) to administer 360 Feedback. The 360 Feedback cycle in CultureAmp exists of a Self review, Manager review and Peer reviews. 

Managers will be assigned as the coach for their direct reports. It is important that all managers review and send out the 360 feedback within 48 hours once the survey closes and you are sent the results.

- If a team member is in the process of migrating to a new role, the current manager and new manager should arrange a successful handover of the feedback, whether sync or async.
- If your manager changes throughout the 360 process, please contact [People Connect](https://about.gitlab.com/handbook/people-group/people-connect/) to make this change on Culture Amp.

We have recorded a training overview of the 360 process via Culture Amp for your review and created a slide-deck to provide [guidance on 360-Feedback](https://docs.google.com/presentation/d/1YxnAWDO0GPSWX5hDepgwMOohT0ZsGzqmUHxvBpaHf7E/edit#slide=id.g77644d9eff_0_0), kindly review before you give feedback.
<figure class="video_container"><iframe src="https://www.youtube.com/embed/zQvNeUX-x5I"></iframe></figure>

### Goal of 360 feedback 
Feedback received in a 360 review can be used for organizational growth, team development and your own personal development. Here are 5 ways that team members can benefit from a 360 review.

- Increased Self Awareness - Gives a team member insight into their behavior and how they are perceived by others. They have a deeper understanding when you compare your self review with the review of your team members.
- Balanced View - Since feedback is received from team members across GitLab and not just your manager it provides a fair and more accurate picture to the team member.
- Leverages Strengths - A 360 helps you identify your strengths in which you can then build your development plan for continued growth and success.
- Uncovers Blind Spots - This enables a team member to understand behaviors they are exhibiting, but never noticed themselves. Highlighting these blind spots allows a team member to focus on those overlooked behaviors.
- Development of Skills - A 360 gives a team member a starting point for creating a development plan. It encourages individual accountability and gives team members control over their own career path.

A 360 review also ties into our core values of Collaboration, Efficiency and Transparency.

#### 360 questions

During this 360 review we will be using the Individual Effectiveness 360 survey. This follows a Start, Stop, Continue format. The questions in the survey are: 

- Start: What is one thing that (X or you) could start doing to help your team to succeed?
- Continue: What do you believe (X or you) excel(s) at that should be continued?
- Stop: What do you believe are (X's or your) biggest opportunities to improve that could make a real difference?
- Open question: Please add anything else, not listed above, that you'd like to continue, improve or do more of.

#### Multiselect options
To help guide team members in giving feedback, CultureAmp is offering options under the Start, Stop, Continue questions. When giving feedback you can select up to three options that you would say are applicable. If there are no options that match your feedback please use the option "Other". Hereafter you can elaborate in the comments. 

The options shown are: 
- Collaboration: Communicating information, ideas and concepts clearly. Working well with a range of people from across the business.
- Feedback: Actively giving, seeking and responding positively to thoughtful feedback.
- Inclusion: Involving others. Inviting people to contribute their opinions and ideas.
- Iteration: Experimenting with innovative ideas and approaches / being open to new ways of doing things.
- Results: Producing high quality work. Providing practical solutions to problems and finding ways to make improvements. Taking the lead and showing ownership of issues. Demonstrating a bias for action.
- Growth mindset: Eager to learn and committed to ongoing development.
- Efficiency: Prioritizing work and managing time well.
- Transparency: Actively keeping people informed about what is happening. Being open and transparent.
- Technical Competence: Demonstrating a high level of domain, functional and/or technical capability. 
- Other. 

### Reviewer Nomination Process 
In Culture Amp, a reviewer is anyone who is requested to provide feedback for a team member. Reviewers can be managers, direct reports or co-workers. Here are some best practices for selecting reviewers:

- Nominate your manager, all of your direct reports (if applicable) and a selection of up to three peers. If you are in heavily cross-functional or leadership roles, ensure you have cross-functional representation in line with your level of collaboration.
- Search for team members using their email or last name, as CultureAmp does not support listing Preferred Name, only Legal Name and Email.
- Reviewers need to be in a position to provide meaningful feedback, supported by examples, that will help you find a focus.
- Choose people who you have worked closely with for at least 3 months. An exception is where you may have worked very closely with someone, say on a project, but for a shorter amount of time.
- Select reviewers who will provide you with honest and perhaps even "difficult to hear" feedback.

Remember you want to hear honest feedback so select reviewers you know will provide you meaningful data. In general, comments should be to the point and include specific examples. Helping your team member understand the impact of what they are, or are not doing, is an important part of making that feedback actionable. Feedback will be anonymous, however each team member is given a unique email link to provide feedback so please do not share. In Culture Amp reviewers are listed in 3 categories:

- Manager
- Direct Reports
- Co-worker

Feedback will not be tied back to a specific reviewer, however you only have 1 manager so you know that any feedback in that section came from directly from your manager. If you nominated 10 team members to provide feedback all 10 will be listed however any feedback is not tied directly back to a specific team member.

## Training Material on Giving and Receiving Feedback
Below there's an extensive list with existing content that has been created for giving and receiving feedback. Before participating in the 360 Feedback process we recommend to review the below content: 
- [The importance of Feedback](/handbook/people-group/guidance-on-feedback/)
- [Giving and Receiving Feedback including CEO Learning Session](/handbook/people-group/guidance-on-feedback/#guidance-on-giving-and-receiving-feedback)
- [Responding to Negative Feedback](/handbook/leadership/#responding-to-negative-feedback)
- [Radical Candor](/handbook/people-group/guidance-on-feedback/#radical-candor)
- [SBI Model](/handbook/people-group/guidance-on-feedback/#s-b-i-model)
If you want to further learn, we recommend watching these recorded Live Learning Sessions:
- [Live Learning Session on Delivering Feedback](/handbook/people-group/guidance-on-feedback/#live-learning-session-on-delivering-feedback)
- [Live Learning Session on Receiving Feedback](/handbook/people-group/guidance-on-feedback/#live-learning-session-on-receiving-feedback)


### Giving Feedback in 360s
Besides the above content this section will talk about giving feedback specifically in the 360 Feedback process.

If you feel overwhelmed by the number of team members that have requested feedback from you, keep in mind that you are providing your team members with a gift: the ability to learn and grow from the feedback they receive. However, you may not have feedback related to each of the questions asked. That is ok. If you don't have anything meaningful to provide, you can put not-applicable. Focus on the team member and the questions for which you have meaningful and helpful feedback. If you feel like you are not in a position to complete a meaningful review in general, please let the team member know as soon as possible, so they can select another reviewer instead. 

**General Tips:**

- It is ok to skip a question by typing N/A if you don't have meaningful feedback in that area. Don't create a "story" where there is none.
- Prepare - Think about the individual beforehand. What do you value in them as a co-worker? Where do you think are their biggest opportunities to improve?
- Speak from your own experience - avoid "I've heard..." statements.
- Be specific - provide examples wherever possible and avoid general statements like "really good" or "difficult to work with"
- Keep it actionable- always describe behaviors, not traits. Focus on what the person can actually do something about going forward (i.e. more of, less of, keep doing - "it would be good to see more of X as it leads to Y".)
- Be respectful AND honest - development feedback can at times be challenging to give. Keep in mind the purpose of the survey is for development and not to judge or evaluate performance.
- Try to explain the why? Thinking through the "why" of the feedback will help you provide better and more meaningful feedback.
- Try to avoid comparing different team members and rather look at a person's progress, comparing them against themselves only.
    - Ex: Person X's performance in January compared to their performance in May, as opposed to person X's performance compared to person Z's performance.
- Team members have very different backgrounds, personalities, strengths and other elements that dictate their performance. The most important thing is that they are making progress and this should be encouraged.
- Try to complete feedback for 1 or 2 people a day, versus waiting until the last minute. Spending 10-20 minutes a day on this can make thinking through and writing the feedback less overwhelming.
- If you have submitted your requests for feedback in Culture Amp and need to make a change (add or remove a reviewer(s)), please reach out to [People Connect](https://about.gitlab.com/handbook/people-group/people-connect/)with the requested changes and we will update your profile if still within the deadline where changes can occur.

#### Bias

- At this moment in time we do not have any integration tools in Culture Amp to help us combat bias. As an alternative and to help with this we have 2 Culture Amp training courses. One for [team members](https://www.cultureamptraining.com/participate-in-a-performance-cycle-for-employees/481210) and the other for [managers](https://www.cultureamptraining.com/performance-for-managers/437360). These training courses will combat bias.

### Receiving Feedback in 360s

Be open to engaging in the conversation. Your peers have taken the time to provide you with their feedback. And the purpose of this feedback is to help you develop and reach your full potential. The perception they have of you is important information for you to have and to build into an action plan.

Before going into the conversation and reviewing feedback, check out the page on [receiving feedback](/handbook/people-group/guidance-on-feedback/#receiving-feedback).

Be accepting of positive feedback. Instead of deflecting compliments, hear and internalise them. They are strong indicators of where you have successfully developed your skills. Remember to maintain a focus on them so you can continue to develop the skill.

During the 360 Feedback meeting:
- Breathe!
- Assume positive intent
- Avoid your first response as chances are it may be defensive
- Ask lots of questions to fully understand the feedback
- Ask for time to process the feedback and come back with any follow up questions & / or action
- Say thank you for their time

If you would like to learn more, we held a [Receiving Feedback Live Learning course](/handbook/people-group/guidance-on-feedback/#receiving-feedback) on 2020-02-25.

### Guidance for Managers
The feedback that your team member receives may reinforce excellent or under performance you have already observed, but shouldn't be the only data point you use in evaluating performance. In cases where you’ve identified your top performer, we should learn from what makes that person successful to share with others. In cases where unsatisfactory performance is identified, you should also address that timely and honestly. The feedback included through the 360 process may help in talking through examples of strengths or improvement areas, but you should not wait for the 360 Feedback process to address performance matters.

### Sharing the report as a Manager
Once the survey closes for managers we advise to send out the completed review to their direct reports within 48 hours. Managers and the team member should schedule time within the next 2 weeks to go over the results and as a follow up can create a development plan. 

Giving and receiving feedback can be hard for both the manager and the team members, remember to be open minded and calm. Be open minded to the fact that others may see something that you do not. If you disagree with the feedback, others may be seeing something that you are not aware of- we called these blind spots earlier. Allow for the fact that other may be right, and use that possibility to look within yourself. Managers, feedback should never be a surprise! It is meant to guide, mentor, support, enhance and to help the team member grow. Try and maintain the model that feedback is a gift- it is data. More data is always better because it provides us with choices we wouldn't otherwise have.

### 360 Feedback Conversation

This section outlines the conversation for a manager to have with their direct report after the cycle has closed. Peer feedback is not meant to evaluate performance specifically, but instead assist in giving feedback to help the team member grow and develop within their role.

We recorded a training on this subject:
<figure class="video_container"><iframe src="https://www.youtube.com/embed/VK8cA8nYcoY"></iframe></figure>

- No surprises. Team members should not hear about positive feedback or performance in need of improvement for the first time at the 360 feedback meeting. Team members should have regular [1:1s](/handbook/leadership/1-1/) where this is discussed. However, if new information is uncovered during the 360 Feedback process, you should discuss that new data.
- The overall aim is providing meaningful feedback. Don't allow the feedback meeting (document and conversation) to (d)evolve into a "todo" list.
- Managers should send the results within 48 hours of the survey closing so they can prepare and come to the meeting with questions and discussion points.
- Make sure you (Manager) are also prepared for the discussion, write down some notes and key points you want to make. What are the major themes coming out of the feedback?
- Make time to talk about the future [career development](/handbook/leadership/1-1/#career-development-discussion-at-the-1-1) and development opportunities. 
- This should be a conversation, try to avoid doing all the talking and get feedback from the team member. As a manager, you can help your team member process and understand the feedback, helping to avoid over/under reactions or defensiveness. Ask questions such as:
    1. Is there feedback that you received that is surprising or upsetting to you?
    1. After reading your feedback, what are the areas you would like to focus on and how can I help?
    1. How can I be a better manager for you?
    1. What are you hoping to achieve at GitLab this coming year?
- Avoid the [horns and halo effects of recent events](https://www.thebalance.com/effective-performance-review-tips-1918842), and instead make sure to take a step back to review the entire review period.
- Make sure you discuss _positive_ aspects of performance, but avoid using the ["feedback sandwich"](https://www.officevibe.com/blog/employee-feedback-examples) to mask an honest conversation about areas that need improvement
- Follow Up. Did you discuss pathways to career progress, or specific points of attention for improving performance? Make sure you add them to the top of the 1:1 doc so as to remind yourselves to follow up every so often.
- Managers should also share the themes of the feedback they received with their teams. Making yourself open and vulnerable can help the rest of the team understand that it is ok to get hard feedback and we can grow from it. It also enlists the team is helping you grow.
- Consider asking each team member to share the top 2-3 Themes from their feedback, what they plan to do now, and how the team can help.
- If there are areas that were indicated over the 360s that need immediate improvement, are not aligned with our values or go against our [Code of Conduct](/handbook/legal/gitlab-code-of-business-conduct-and-ethics/#viii-questions-reporting-and-effect-of-violations), please reach out to our [Team Member Relations Specialist](https://about.gitlab.com/handbook/people-group/) via teammemberrelations@gitlab.com.

### Action Plans

Once feedback has been delivered, a manager and direct report both have the option to create an Action Plan in Culture Amp. This part of the process is up to the manager’s discretion, and you may also choose to follow up in 1:1 meetings or via other processes.

#### Action Plan in Culture Amp

- Have your direct report click the flag icon to the left of any items they want to work on. This makes those items available in the “Action plan” interface.
- Discuss with your direct report and choose one area to focus on. Managers and their direct reports have identical views and options at this step.
- Work together to create an action title, description, and due date. Once submitted, this will appear as the “focus” item in the Action Plan area.

Examples of actions to take following the conversation and depending on area of focus:

- Attend a training / webinar
- Shadow a team member
- Connect with a mentor
- Complete a course
- Read an article / book on the topic
- Take on a project / task for exposure

### Sharing 360 Feedback outside of the 360 Feedback Cycle

If a team member becomes a manager to direct reports after the 360 Feedback cycle has completed, and would like to see their new direct reports' 360 Feedback reports, the new manager must reach out to the direct reports directly to ask permission for either the previous manager to download and send the reports, or for the direct reports to download their own reports and send them directly to the new manager. Any approval should be in writing by email, not Slack.

## Common Questions 

Troubleshooting for common questions during the 360 process. 

1. I had a manager update during the 360 process, and I would like them to be my coach, how can I update this? 
- Reach out in `#people-connect` and request a coach update. Please provide the new coach's email address. 

1. I will be out on PTO during the nomination period; what can I do? 
- You have two options 
  1. Your manager/coach can update your nominations in CultureAmp -Or-
  1. You can email `#people-connect` with the email addresses of your nominations. 


