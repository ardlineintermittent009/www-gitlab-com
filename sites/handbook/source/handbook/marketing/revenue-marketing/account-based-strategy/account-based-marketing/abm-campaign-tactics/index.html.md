---
layout: handbook-page-toc
title: "ABM Campaign Tactic Processes"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## ABM Campaign Tactic Processes
The Account Based Strategy team uses various tactics when launching an Account Based Marketing (ABM) campaign. This page will provide the details an ABM Manager needs to successfully launch those tactics.

## Launching an ABM Campaign
When launching a 1:1 or 1:Few ABM campaign, the ABM Manager will open an issue for each account as a SSOT for tactics, launch schedule and bi-weekly metric updates. The ABM Manager will also open an issue for each tactic being launched for the account and relate to the larger campaign issue. 
- [ABM Account Main Issue](https://gitlab.com/gitlab-com/marketing/account-based-strategy/account-based-marketing/-/issues/new?issuable_template=ABM_Account_Campaign_Tracking_Issue)
- [Issue templates for ABM Tactics](https://gitlab.com/gitlab-com/marketing/account-based-strategy/account-based-marketing/-/tree/master/.gitlab/issue_templates) 

## Account Based Marketing Campaign Tactics

### Demandbase Display ads
The ABS team leverages the tool [Demandbase](/handbook/marketing/revenue-marketing/account-based-strategy/demandbase/), a targeting and personalization platform, that lets us target online ads to companies that fit pre-determined criteria. This means delivering advertising assets to audiences in specific accounts, with custom messaging and offers informed by the account's ABM strategy and needs.

#### Launching a Demandbase Campaign as part of an ABM Account Strategy
1. Open the [Demandbase campaign issue](https://gitlab.com/gitlab-com/marketing/account-based-strategy/account-based-marketing/-/issues/new?issuable_template=ABM_Demandbase_Campaign).
1. Create target audience account list in Demandbase.
1. Create display ads in Canva (sizing below).
1. Share screenshots of Display ads with account team in main campaign issue for approval.
1. Create PathFactory track or learn page (instructions [here](/handbook/marketing/revenue-marketing/account-based-strategy/account-based-marketing/abm-campaign-tactics/#pathfactory)).
1. Launch campaign and add both reach test (full report download) and tear sheet CSV to the issue

#### Demandbase Display Ad Specs
- 728x90
- 300x250
- 160x600
- 300x600
- 970x250
- 320x50
- 300x50

### PathFactory 
Detailed information about PathFactory, creating tracks/explore pages, etc. can also be found [here](/handbook/marketing/marketing-operations/pathfactory/). 

##### Content Track
A collection of content specifically selected based on industry, tech stack, entry point use case, SFDC notes and conversations with the account team. PathFactory leverages machine-learning to suggesting the ideal content in the track based on what prospects are already viewing.
1. Determine if you want to create a [target track or a recommended track](/handbook/marketing/marketing-operations/pathfactory/#best-practices). Click `Campaign Tools` at the top of the PathFactory site. Select target or recommended. 
1. Navigate to the ABM/ABS folder and click `+ Create Track` in the top right OR click a track that you would like to clone. If you are cloning a track, once you have clicked in, in the top left next to the track name click the clone track symbol of two overlaping squares. 
1. Name your track with the official [naming convention](/handbook/marketing/marketing-operations/pathfactory/#tracks) and add ABM after the square brackets to the front of the campaign name. 
1. Click `+ Add Content` in the top right and add content as needed. Remove content by selecting the piece you want to remove and clicking the trash can icon at the bottom right. 
1. Ensure you have the correct [form strategy](/handbook/marketing/marketing-operations/pathfactory/#form-strategy) enabled.

##### Explore/Learn Page
A landing page that is customized to the account with a logo, SAL image and specific banner. The page will then show the content treack created above as options to click into.
1. Follow the steps above to create a content track. 
1. Click `Campaign Tools` at the top of the PathFactory site. Select explore. 
1. Click `+ Create Explore Page` and selec the track you created that you want to be leveraged in this explore page. Select the ABM/ABS folder. 
1. Navigate to `Appearances` by selecting the gear icon at the top right of PathFactory. The colors, fonts, imagery, and layout of your explore page can be customized there.
1. Navigate back to the ABM Explore folder and select the page you are creating. Add the appearance you just created at the right hand side. 
1. Update the URL slug
1. Test your page by clicking URL slug and using the whole link + the slug name. 

##### Best Practices
- 5-7 assets per track (can do more for explore pages and middle of funnel)
- For 1:1 accounts, if we have the intel to use a target track 3-4 assets
- Typically don't start with a gated asset
- Good to include multiple content types (blog, video, whitepaper, etc.)

### Drift
Drift is a chat platform used by SDRs to engage visitors on select webpages. When a site visitor interacts with the bot they will be taken through a customized series of questions offering the option to view the PathFactory track, chat with an SDR or schedule time to speak with an SDR.

We can create a specific welcome message, playbook, or chat landing page for ABM accounts. More information can be found here. To create any of the above open a [Drift Campaign Issue](https://gitlab.com/gitlab-com/marketing/marketing-operations/-/issues/new?issuable_template=Drift_Campaign_Request) for our Marketing Operations team.

### Terminus Signature Banner
SDRs will have a signature banner that aligns with the ads prospects are being served. It will only send an ABM campaign banner if they are reaching out to someone from one of our ABM accounts. If a prospect clicks through, they will be redirected to the PathFactory track. Additional information about Terminus can be found [here](/handbook/marketing/marketing-operations/terminus-email-experiences/).

1. Ensure SDR team is aware of launch. 
1. Create a CSV of accounts you want to target. Account lists require a header with either 'website', 'url', or 'domain' in order to import correctly. 
1. Create the banner you would like shown in SDR emails in Canva. Size: 900 wide x 240 pixels tall.
1. Login to Terminus and click the list icon at the left hand side.
1. Select `import list`
1. To target specific accounts, select CSV Upload. 
1. Drop in your CSV and import. 
1. Click campaigns icon at the left hand side - the flag icon.
1. In the top right-hand corner, select `create campaign`.
1. Typically, for ABM campaigns we use Targeted Campaigns in Terminus. Select Targeted Campaign and add campaign name at the bottom. 
1. Select your CSV list.
1. Add your banner image and the clickthrough URL (where you want clicking the ad to take a prospect).
1. Add start and end dates and launch.

### Webcasts
Additional information about webcasts and launch steps can be found [here](/handbook/marketing/revenue-marketing/field-marketing/workshop-webcast-how-to/). 

### Buyer Progression Webcasts 
This is a GitLab-hosted virtual event with Webcast type configuration in Zoom. This is the preferred setup for larger GitLab-hosted virtual events (up to 1,000 attendees) that requires registration due to the integration with Marketo for automated lead flow and event tracking. GitLab-hosted webcast type is a single room virtual event that allows multiple hosts. Attendees cannot share audio/video unless manually grated access. 

### Self-Service Webcasts
This is a light-weight virtual event that can be hosted on any team member’s personal Zoom. This is recommended for smaller virtual events (200 attendees max) and allows you to break the audience into smaller groups during the event. Attendees are able to be interactive in this event type, having the option to share both audio/video if allowed by the host. We can track registration, but there is NO Marketo integration, which requires manual list upload to Marketo post-event.

### ABM & Verticurl  
The ABS team will be working with the [Verticurl](/handbook/marketing/demand-generation/campaigns/agency-verticurl/) team to execute email marketing setup and other tasks in Marketo. We will use issues and an issue board to remain aligned. The triage process will be fairly straightforward with the ABM Manager opening and completing all necessary tasks within issues and handing off to the final copy to the Verticurl team.

##### Labels and Issue board

- [**ABM and Verticurl Issue board**](https://gitlab.com/gitlab-com/marketing/account-based-strategy/account-based-marketing/-/boards/2543577)

| Label | Use Case | 
| ------------ | ---------- | 
| ABM-Verticurl::blocked | ABM Manager adds when the issue does not have enough information for Verticurl to work on the issue or the asks are not complete. ABM Manager is to address then re-add the ABM-Verticurl::triage label to flow back through the process. | 
| ABM-Verticurl::wip | ABM Manager adds when the issue is 100% cleaned up and they are ready to send the work to Verticurl to complete | 
| ABM-Verticurl::review | Verticurl adds when they are ready for ABM manager to review the work they have completed | 

#### Webcast SLAs
SLAs for both Buyer Progression and Self-Service Webcasts can be found [here] (https://docs.google.com/spreadsheets/d/1otehEQ1_LPnRs0ilX4CZB4j4qJwBx2t-3R6bZjMTI2Y/edit?usp=sharing)

Please note that all Buyer Progression Webcasts have a **-45 business day** SLA requirement. 

#### Launching a Webcast 
- [ ] Open PMM/TMM [speaker request issue](https://gitlab.com/gitlab-com/marketing/strategic-marketing/product-marketing/-/issues/new?issuable_template=pmm-speaker-request) with your proposed date and topic.
- [ ] Once your speaker is confirmed, ensure the date is still available and add to [Webcast Google calendar](https://calendar.google.com/calendar?cid=Z2l0bGFiLmNvbV8xcXZlNmc4MWRwOTFyOWhldnRrZmQ5cjA5OEBncm91cC5jYWxlbmRhci5nb29nbGUuY29t) when final date is agreed upon - Remember to block the calendar for an additional 30 mins longer before and after the slotted time. Naming Convention for Calendar - [WC Hosted] Name of Webcast and Start-End Time/Time Zone of event (example - 9:00am - 12:00pm PST). If waiting on official confirmation use naming convention `[Hold WC Hosted] Webcast title`
- [ ] Add speaker(s) and team to calendar invite and topic to the description.
- [ ] Uncheck the calendar settings `Modify Event` and `Invite Others` under Guest Permissions so invitees are not able to modify the event or add additional guests.
- [ ] Schedule dry run ~5 days before event and add to [Webcast Google calendar](https://calendar.google.com/calendar?cid=Z2l0bGFiLmNvbV8xcXZlNmc4MWRwOTFyOWhldnRrZmQ5cjA5OEBncm91cC5jYWxlbmRhci5nb29nbGUuY29t) dry run with the same information as well as this epic linked. Only schedule for 60 minutes. Naming Convention for Calendar - [WC Dry Run] Name of Webcast Start-End Time/Time Zone of dry run (example - 9:00am - 10:00am PST)
- [ ] Create Slack channel and invite team
- [ ] If offering incentives, create a campaign tag
