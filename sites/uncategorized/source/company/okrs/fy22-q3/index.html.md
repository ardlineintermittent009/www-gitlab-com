---
layout: markdown_page
title: "FY22-Q3 OKRs"
description: "View GitLabs Objective-Key Results for FY22 Q3. Learn more here!"
canonical_path: "/company/okrs/fy22-q3/"
---

This [fiscal quarter](/handbook/finance/#fiscal-year) will run from August 1, 2021 to October 31, 2021.

## On this page
{:.no_toc}

- TOC
{:toc}

## OKR Schedule
The by-the-book schedule for the OKR timeline would be

| OKR schedule | Week of | Task |
| ------ | ------ | ------ |
| -5 | 2021-06-28 | CEO shares top goals with E-group for feedback |
| -5 | 2021-06-28 | CEO pushes top goals to this page |
| -3 | 2021-07-12 | E-group propose OKRs for their functions in [Epics and Issues nested under the CEO's OKRs](/company/okrs/#executives-propose-okrs-for-their-functions). These issues and epics are shared in #okrs Slack channel|
| -2 | 2021-07-19 | E-group 50 minute draft review meeting |
| -2 | 2021-07-19 | E-group discusses with their respective teams and polish their OKRs |
| -1 | 2021-07-26 | CEO reports post links to final OKR Epics in #okrs slack channel and @ mention the CEO and CoS for approval |
| 0  | 2021-08-02 | CoS updates OKR page for current quarter to be active |


## OKRs

### 1. CEO: GitLab Managed future 
[Epic 1534](https://gitlab.com/groups/gitlab-com/-/epics/1534)
   1. **CEO KR:** X% of [features missing on GitLab.com](/handbook/marketing/strategic-marketing/dot-com-vs-self-managed/.#all-differences-between-gitlab-saas-and-self-managed) have moved to scoped or [viable](/direction/maturity/). [Issue 12066](https://gitlab.com/gitlab-com/www-gitlab-com/-/issues/12066) 
   1. **CEO KR:** Launch first customer facing iteration of three new delivery methods: Horse, Plus and Region. [Issue 12067](https://gitlab.com/gitlab-com/www-gitlab-com/-/issues/12067)
   1. **CEO KR:** Have Workspace replace the admin screen. [Issue 12068](https://gitlab.com/gitlab-com/www-gitlab-com/-/issues/12068)

### 2. CEO: Conversion from free 
[Epic 1535](https://gitlab.com/groups/gitlab-com/-/epics/1535)
   1. **CEO KR:** Cloud licensing for 100% of new and renewing subscriptions. [Issue 12069](https://gitlab.com/gitlab-com/www-gitlab-com/-/issues/12069)
   1. **CEO KR:** Release has a wizard with 100 templates. [Issue 12075](https://gitlab.com/gitlab-com/www-gitlab-com/-/issues/12075)
   1. **CEO KR:** Increase funnel volume to drive pipeline. [Issue 12074](https://gitlab.com/gitlab-com/www-gitlab-com/-/issues/12074)
   1.  **CEO KR:** Increase Trial conversion to X%, Increase Free to Trial/Paid to X%. [Issue 12073](https://gitlab.com/gitlab-com/www-gitlab-com/-/issues/12073)

### 3. CEO: Even prouder to work here
[Epic 1536](https://gitlab.com/groups/gitlab-com/-/epics/1536)
   1. **CEO KR:** 90% of team members answering an internal survey say that they agree that:
      1. Feel comfortable delivering our Golden Pitch
      1. Feel comfortable speaking to our brand message
      1. [If attending] Contribute was a success
      1. We are at mile 3 or 4 of a marathon or ultra-marathon
      1. Love the new GitLab Brand
      1. GitLab pays competitively. [Issue 12070](https://gitlab.com/gitlab-com/www-gitlab-com/-/issues/12070)
   1. **CEO KR:** Meet hiring targets and have a diverse top of pipeline. [Issue 12071](https://gitlab.com/gitlab-com/www-gitlab-com/-/issues/12071)
   1. **CEO KR:** 7 certifications with more than 10,000 certificates issued. [Issue 13](https://gitlab.com/gitlab-com/www-gitlab-com/-/issues/12076)
   1. **CEO KR:** Graduate 2 internal projects from internal limited to public. [Issue 12072](https://gitlab.com/gitlab-com/www-gitlab-com/-/issues/12072)
