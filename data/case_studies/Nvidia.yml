title: Nvidia
cover_image: '/images/blogimages/nvidia.jpg'
cover_title: |
  How GitLab Geo supports NVIDIA’s innovation
cover_description: |
  NVIDIA’s distributed teams rely on Geo for stability and security.
canonical_path: "/customers/Nvidia/"
twitter_image: '/images/blogimages/nvidia.jpg'

twitter_text: 'NVIDIA’s distributed teams rely on GitLab Geo for stability and security.'

customer_logo: '/images/case_study_logos/logo_nvidia_2.svg'
customer_logo_css_class: brand-logo-tall
customer_industry: Technology
customer_location: More than 50 offices worldwide
customer_solution: GitLab Premium
customer_employees: Over 11,000 employees
customer_overview: |
   GitLab Geo helps NVIDIA’s development teams to stay secure and highly communicative.
customer_challenge: |
  Dispersed development groups face mirroring issues -- in time management and security.

key_benefits:
  - |
    Flexible merge request for handling and approvals
  - |
    Geo enforces the same access rules as main cluster
  - |
    Unparalleled LFS support
  - |
    Exceptional scalability with zero downtime
  - |
    More upgrades, more frequently
  - |
    Increased scalability
  - |
    Easy integration

customer_stats:
  - stat: 0
    label: Downtime
  - stat: 51%
    label: User growth in 1 year
  - stat: 99%
    label: Uptime

customer_study_content:
  - title: the customer
    subtitle: A pioneer in supercharged computing
    content:
      - |
        NVIDIA is known for creating the world’s first graphics processing unit (GPU) in 1999, which changed the face of computer gaming.
      - |
        Since then, [NVIDIA](https://www.nvidia.com/en-us/) has grown to be a global leader in visual computing, artificial intelligence, data centers, deep learning, and gaming platforms. The company strives to provide the latest GPU technology for mobile computing, automotive services, medical devices, and gaming on a massive scale. Its GPUs are widely used in the world’s leading public cloud datacenters.

  - title: the challenge
    subtitle: Staying secure, scalable, and seen
    content:
      - |
        NVIDIA has more than 50 offices worldwide with more than 13,000 employees, requiring numerous software applications. Creative freedom is expected. “I think we consider it a competitive advantage, that we don't mandate from the top when it comes to tools and things. We let groups organically figure out how they want to best operate,” said Patrick Herlihy, configuration management specialist at NVIDIA.
      - |
        Encouraging staff to use the best tool for business responsibilities brings a variety of challenges. Security and transparent communication are integral parts of keeping business momentum moving forward. “We'd be wasting a lot of NVIDIA engineering time with lots of individual little servers being managed around the world,” Herlihy said. “We need something more modern, with a modern workflow and features and things,” said Kevin Sage, SCM manager.
      - |
        The company strategy allows teams to use any platform or tool that they prefer. If a tool becomes accepted by a majority, a plan is then put in place to layer in support for the tool. “We have a very decentralized model here where groups kind of get to choose their own way of doing their own things for a while. And then eventually they'll grow big enough, where that becomes unworkable and then they'll come to the central groups, like us, to help them out and manage it for them,” Herlihy said. “So in a sense, it's kind of a marketplace for ideas, and a lot of people are choosing GitLab. If they're given an open choice, they seem to choose GitLab.”


  - blockquote: Without GitLab, we'd be wasting engineering time with lots of individual little servers being managed around the world. We would probably have a lot more headaches and still be suffering with scalability problems.
    attribution: Patrick Herlihy
    attribution_title: Configuration Management Specialist, NVIDIA

  - title: the solution
    subtitle: Keeping dispersed teams on the same page
    content:
      - |
        GitLab’s Community Edition was introduced at NVIDIA in much the same way. While GitLab was introduced internally in 2016, the overall acceptance rate has skyrocketed and is now fully supported. As the tool was utilized by additional people, it became clear that GitLab’s integration capabilities, scalability, and ease of use are elements that not all other tools share. “GitLab is the only Git server that really gives us those capabilities. I think that's been a huge thing for us as the administrators,” Sage said.
      - |
        GitLab Geo is especially critical to enable distributed teams to work efficiently and effectively. [GitLab Geo](/solutions/geo/) reduces the time -- and stress -- it takes for NVIDIA’s distributed development teams to clone and manage projects. “GitLab has continually gotten better with scalability. It's gotten more ability to spread among more nodes. With Geo, within one data center, we can now scale ... We have a bunch of nodes running and sharing the load, and it's all invisible to the users, and it's continued work there to make it scale better, be more fault-tolerant, more high availability,” Sage said. “We're now doing zero downtime upgrades, I mean all that stuff has been really great improvements in the product that makes it easier to run and manage in a large deployment.”
      - |
        GitLab Geo is empowering NVIDIA to easily span the globe and provide services for their international teams. Utilizing GitLab Geo’s read-only mirrors, the company is able to keep data close to users — instead of having them waste hours waiting for large repos to be pulled down to work on them.
      - |
        The end goal is to provide developers with a dedicated, scalable, experience — and prevent users from hitting all of the servers at once. The company is also in the process of establishing additional facilities and GitLab Geo with High Availability capabilities is helping teams be prepared for any disaster recovery needs and maintain their uptime capabilities.
      - |
        GitLab also provides a level of transparency that other tools do not. “The fact that you're so transparent in your development process is huge. It helped me come up to speed relatively quickly. But, also I’m able to understand how the product works internally and be able to actually fix things myself,” Herlihy said.

  - title: the results
    subtitle: Transparency breeds innovation
    content:
      - |
        The goal is to have uptime at 100% and the development teams have found that with GitLab. “For about the last six months, I would say, it's been pretty close to 100%...for the [GitLab HA](/solutions/high-availability/) model to actually never have downtime which is pretty impressive,” Herlihy said. “On the Geo side, there's more usage than I thought.”
      - |
        GitLab’s transparency in communication -- and even failures -- has created a safe environment for NVIDIA’s development teams. It is no secret that software isn’t reliable 100% of the time. However, GitLab is quick to point out issues, and even faster at fixing them. “When you have a problem, we can get it fixed. You know, we can get help, we don't have to wait three years for someone in the community to decide to submit a patch,” Sage said. “Good support has been a really big deal for us.”
      - |
        [GitLab’s openness](/handbook/values/) has been appreciated in a company culture capacity as well. Not just in the way that the tool is managed, but how clear communication improves processes -- both internally and for the customer. “We've had senior directors who are using GitLab as an example of why we want transparency, and how to use transparency, and how much it helps people,” Sage said. “The way you guys handle that kind of stuff is actually being noticed by our senior management, and they’re guiding us that we should try and copy some of that with our internal applications and tools too.”
      - |
        NVIDIA’s next big step with GitLab is pushing forward with disaster recovery planning strategies. “Disaster recovery is our plan using Geo. So, that should be an easy cut over...we are trying to make it a lot easier to not so much have automated failover, but make the disaster recovery part of Geo work really easily,” Herlihy said.
